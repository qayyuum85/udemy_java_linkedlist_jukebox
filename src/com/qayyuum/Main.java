package com.qayyuum;

import java.util.*;

public class Main {
    private static ArrayList<Album> albums = new ArrayList<Album>();

    public static void main(String[] args) {
        // write your code here
        Album albumFotograf = new Album("Fotograf", "Shamrin");
        albumFotograf.addSong("Di Alam Fana Cinta", 5.30d);
        albumFotograf.addSong("Luka Seribu Rindu", 5.41d);
        albumFotograf.addSong("Ke Pintu Kasihmu", 4.15d);
        albums.add(albumFotograf);

        Album albumXPDC = new Album("Kompilasi Terbaik XPDC", "XPDC");
        albumXPDC.addSong("Titian Perjalanan", 6.03d);
        albumXPDC.addSong("Semangat yang Hilang", 5.34d);
        albumXPDC.addSong("CT Cinta Kenangan Silam", 6.32d);
        albums.add(albumXPDC);

        LinkedList<Song> playList = new LinkedList<Song>();
        albums.get(0).addToPlaylist("Titian Perjalanan", playList);
        albums.get(0).addToPlaylist("Luka Seribu Rindu", playList);
        albums.get(1).addToPlaylist("Titian Perjalanan", playList);
        albums.get(1).addToPlaylist("CT Cinta Kenangan Silam", playList);

        play(playList);

    }

    private static void play(LinkedList<Song> playList) {
        Scanner scanner = new Scanner(System.in);
        boolean quit = false;
        boolean forward = true;
        ListIterator<Song> listIterator = playList.listIterator();
        if (playList.size() == 0) {
            System.out.println("No song in playlist.");
            return;
        } else {
            System.out.println("Now playing " + listIterator.next().toString());
            printMenu();
        }

        while (!quit) {
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action) {
                case 0:
                    System.out.println("Playlist complete");
                    quit = true;
                    break;
                case 1:
                    if (!forward) {
                        if (listIterator.hasNext()) {
                            listIterator.next();
                        }
                        forward = true;
                    }
                    if (listIterator.hasNext()) {
                        System.out.println("Now playing " + listIterator.next().toString());

                    } else {
                        System.out.println("We have reached the end of playlist.");
                        forward = false;
                    }                 break;
                case 2:
                    if (forward) {
                        if (listIterator.hasPrevious()) {
                            listIterator.previous();
                        }
                        forward = false;
                    }
                    if (listIterator.hasPrevious()) {
                        System.out.println("Now playing " + listIterator.previous().toString());
                    } else {
                        System.out.println("We have reached the start of playlist.");
                        forward = true;
                    }
                    break;
                case 3:
                    if (forward) {
                        if (listIterator.hasPrevious()) {
                            System.out.println("Now replaying " + listIterator.previous());
                            forward = false;
                        } else {
                            System.out.println("We are at the start of the list.");
                        }
                    } else {
                        if (listIterator.hasNext()) {
                            System.out.println("Now replaying " + listIterator.next());
                            forward = false;
                        } else {
                            System.out.println("We are at the end of the list.");
                        }
                    }
                    break;
                case 4:
                    printList(playList);
                    break;
                case 5:
                    printMenu();
                    break;

                case 6:
                    if (playList.size() > 0) {
                        listIterator.remove();
                        if (listIterator.hasNext()) {
                            System.out.println("Now playing " + listIterator.next());
                        } else if (listIterator.hasPrevious()){
                            System.out.println("Now playing "+ listIterator.previous());
                        }
                    }
            }
        }
    }

    private static void printMenu() {
        System.out.println("Available actions:\npress");
        System.out.println("0 - to quit\n" +
                "1 - to play next song\n" +
                "2 - to play previous song\n" +
                "3 - to replay current song\n" +
                "4 - list songs in playlist\n" +
                "5 - show available actions\n" +
                "5 - delete current song from playlist.");
    }

    private static void printList(LinkedList<Song> playList) {
        ListIterator<Song> listIterator = playList.listIterator();
        System.out.println("==============================");
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
        System.out.println("==============================");
    }
}
